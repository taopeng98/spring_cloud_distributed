package com.tp.user_server2.bank.service.impl;

import com.tp.user_server2.bank.model.User;
import com.tp.user_server2.bank.mapper.UserMapper;
import com.tp.user_server2.bank.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户表 服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-03-22
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
