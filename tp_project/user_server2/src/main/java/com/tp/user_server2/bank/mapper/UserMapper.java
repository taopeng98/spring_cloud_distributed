package com.tp.user_server2.bank.mapper;

import com.tp.user_server2.bank.model.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账户表 Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-03-22
 */
public interface UserMapper extends BaseMapper<User> {

}
