package com.tp.user_server2.bank.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tp.user_server2.bank.model.AccountInfo;

/**
 * <p>
 * 账户表 服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-02-25
 */
public interface IAccountInfoService extends IService<AccountInfo> {

}
