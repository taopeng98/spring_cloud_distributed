package com.tp.user_server2.bank.service;

import com.tp.user_server2.bank.model.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 账户表 服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-03-22
 */
public interface IUserService extends IService<User> {

}
