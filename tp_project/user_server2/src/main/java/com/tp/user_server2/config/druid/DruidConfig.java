//package com.tp.user_server2.config.druid;
//
//import com.alibaba.druid.pool.DruidDataSource;
//import io.seata.rm.datasource.DataSourceProxy;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//
//import javax.sql.DataSource;
//
//@Configuration
//public class DruidConfig {
//
//    private final ApplicationContext applicationContext;
//
//
//    public DruidConfig(ApplicationContext applicationContext) {
//        this.applicationContext = applicationContext;
//    }
//
//    @Bean(name = "dataSource")
//    @Primary
//    @ConfigurationProperties(prefix = "spring.datasource.ds0")
//    public DruidDataSource ds0(){
//        DruidDataSource druidDataSource = new DruidDataSource();
//        return druidDataSource;
//    }
//
//    @Bean
//    public DataSource dataSource(DruidDataSource druidDataSource){
//        DataSourceProxy pds0 = new DataSourceProxy(druidDataSource);
//        return pds0;
//    }
//}
