package com.tp.user_server2.bank.controller;


import com.tp.user_server2.bank.model.AccountInfo;
import com.tp.user_server2.bank.service.IAccountInfoService;
import com.tp.user_server2.fegin.UserFeginInterface;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 账户表 前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-02-25
 */
@Controller
@RequestMapping("/account-info")
public class AccountInfoController {

    //注入feign接口
    @Autowired
    private UserFeginInterface userFeginInterface;

    @Autowired
    private IAccountInfoService accountInfoService;

    @ResponseBody
    @GetMapping(value = "/get")
    public String get(){

        //feign调用
        String str = userFeginInterface.get();

        if(StringUtils.equals(str,"服务不可用")){
            return "sssssssssss";
        }

        return str;
    }

    @ResponseBody
    @GetMapping(value = "/getAccount")
    public String getAccount(){

        AccountInfo accountInfo = accountInfoService.getById(2);
        return accountInfo.getAccountName();
    }


    @ResponseBody
    @GetMapping(value = "/saveAccount")
    public String saveAccount(){

        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setAccountName("王五");
        accountInfoService.save(accountInfo);

        AccountInfo accountInfo1 = new AccountInfo();
        accountInfo1.setAccountName("赵六");
        accountInfoService.save(accountInfo1);
        return "1";
    }
}
