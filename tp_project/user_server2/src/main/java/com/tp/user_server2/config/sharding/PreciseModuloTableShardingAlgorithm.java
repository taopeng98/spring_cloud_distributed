package com.tp.user_server2.config.sharding;

import com.tp.user_server2.config.TimeUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.Date;

/**
 * @author xuzhiyong
 * @createDate 2019-01-28-22:30
 * 按表
 */
public class PreciseModuloTableShardingAlgorithm implements PreciseShardingAlgorithm<Date> {
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<Date> preciseShardingValue) {
        //对于库的分片collection存放的是所有的库的列表，这里代表user01~user12
        //配置的分片的sharding-column对应的值
        String timeValue =  TimeUtil.getTimeStr(preciseShardingValue.getValue());
        //分库时配置的sharding-column
        String time = preciseShardingValue.getColumnName();
        //需要分库的逻辑表
        String table = preciseShardingValue.getLogicTableName();
        if(StringUtils.isBlank(timeValue)){
            throw new UnsupportedOperationException("preciseShardingValue is null");
        }
        //按月路由
        for (String each : collection) {
            String value = StringUtils.substring(timeValue,5,7); //获取到月份
            System.out.println("------------------------------------------------"+value);
            if(each.endsWith(value)){
                //这里返回回去的就是最终需要查询的表名
                return each;
            }
        }
        return null;
    }
}