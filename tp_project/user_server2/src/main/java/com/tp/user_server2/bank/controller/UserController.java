package com.tp.user_server2.bank.controller;


import com.tp.user_server2.bank.model.AccountInfo;
import com.tp.user_server2.bank.model.User;
import com.tp.user_server2.bank.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 账户表 前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-03-22
 */
@RestController
@RequestMapping("/user0")
public class UserController {
    @Autowired
    private IUserService userService;

    @ResponseBody
    @GetMapping(value = "/saveUser")
    public String saveAccount(){

        User user = new User();
        user.setAccountName("张三");
        user.setCreateTime(new Date());
        userService.save(user);

        return "1";
    }
}
