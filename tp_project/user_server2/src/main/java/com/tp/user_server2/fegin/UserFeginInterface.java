package com.tp.user_server2.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @FeignClient 标志开启feign客户端
 * value = "USER-SERVER"  调用生产者服务名
 * fallback = UserServiceHystrix.class  生产者异常后调用的熔断代码类
 *
 */
@FeignClient(value = "USER-SERVER",fallback = UserServiceHystrix.class)
public interface UserFeginInterface {

    /**
     * @GetMapping("/account-info/get")  生产者接口名
     * @return
     */
    @GetMapping("/account-info/get")
    public String get();
}
