package com.tp.user_server2.config;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {

    public static String getTimeStr(Date time){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return sdf.format(time);
    }
}
