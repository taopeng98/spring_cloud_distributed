//package com.tp.user_server;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Optional;
//import java.util.function.Supplier;
//
//public class Test {
//
//    public static String chooseMethod(String type,String data){
//        type = Optional.ofNullable(type).filter(b -> b.equals("1") || b.equals("2")).orElse("3");
//
//        Map<String, Supplier<?>> supplierMap = new HashMap<>();
//
//        Supplier<String> supplier1 = () -> getString1(data);
//        Supplier<Integer> supplier2 = () -> getString2(data);
//        Supplier<Float> supplier3 = () -> getString3(data);
//
//        supplierMap.put("1",supplier1);
//        supplierMap.put("2",supplier2);
//        supplierMap.put("3",supplier3);
//
//        return String.valueOf(supplierMap.get(type).get());
//    }
//
//
//    public static void main(String[] args) {
//        String str = chooseMethod("4","4");
//        System.out.println(str);
//    }
//
//    public static String getString1(String type){
//        return "1" + type;
//    }
//
//    public static Integer getString2(String type){
//        return 2 + Integer.valueOf(type);
//    }
//
//    public static Float getString3(String type){
//        return 3.00f + Float.valueOf(type);
//    }
//}
