package com.tp.user_server2.bank.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.tp.user_server2.bank.model.AccountInfo;
import com.tp.user_server2.bank.service.IAccountInfoService;
import com.tp.user_server.config.redis.RedisShardedPoolUtil;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 账户表 前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-02-25
 */
@Controller
@RequestMapping("/account-info")
public class AccountInfoController {
    @Autowired
    private IAccountInfoService accountInfoService;
    @Autowired
    private RedisShardedPoolUtil redisShardedPoolUtil;

    @Autowired
    private RedissonClient redissonClient;

    @Value("${server.port}")
    private String port;


    /**
     * 存储缓存数据
     * @param jsonObject
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/redisOption")
    public String redisOption(@RequestBody JSONObject jsonObject){
        Map<String,String> map = JSONObject.toJavaObject(jsonObject,Map.class);
        //循环转换
        for (Map.Entry<String, String> entry : map.entrySet()) {
            redisShardedPoolUtil.setEx(entry.getKey(), entry.getValue(),1111111111);
        }
        return null;
    }

    /**
     * 获取缓存数据
     * @param jsonObject
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/redisOptionGet")
    public String redisOptionGet(@RequestBody JSONObject jsonObject){
        Map<String,String> map = JSONObject.toJavaObject(jsonObject,Map.class);
        //循环转换
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(redisShardedPoolUtil.get(entry.getKey()));
        }
        return "111111111111111";

    }

//    @ResponseBody
//    @GetMapping(value = "/get")
//    public String get(){
//
//       AccountInfo accountInfo = accountInfoService.getById("1");
//        return accountInfo.getAccountName();
//    }

    @ResponseBody
    @GetMapping(value = "/get")
    public String get() throws Exception {
        AccountInfo accountInfo = accountInfoService.getById("1");
        return accountInfo.getAccountName() + port;
    }

    @ResponseBody
    @PostMapping(value = "/reduce")
    public String reduce(@RequestBody JSONObject jsonObject){
        String deductionExpenses = jsonObject.getString("deductExp");
//        accountInfoService.saveOrUpdate();
        return null;
    }



    @ResponseBody
    @PostMapping(value = "/redisOptionReduce")
    public String redisOptionReduce(@RequestBody JSONObject jsonObject) throws ExecutionException, InterruptedException {
        String returnStr = "";
        RLock lock = redissonClient.getLock("xxxxxxxxxxxxxxxxxx");
        boolean isLock = lock.tryLock(3,5,TimeUnit.SECONDS);
        if(isLock){
            AccountInfo accountInfo = accountInfoService.getById(1);
            if(accountInfo.getAccountBalance() > 0){
                Thread.sleep(30); //延长程序操作时间增加线程并发操作时，多个线程获取相同余额的可能性
                double accountBalance = accountInfo.getAccountBalance() - 1;
                UpdateWrapper<AccountInfo> accountInfoUpdateWrapper2 = new UpdateWrapper<>();
                accountInfoUpdateWrapper2.setSql("account_balance = account_balance - 1");
                accountInfoService.update(accountInfo,accountInfoUpdateWrapper2);
                returnStr = "当前剩余余额：" + accountBalance;
            }else {
                returnStr = "当前剩余余额： 0";
            }
            lock.unlock(); //释放锁
        }
        return returnStr;
    }

}
