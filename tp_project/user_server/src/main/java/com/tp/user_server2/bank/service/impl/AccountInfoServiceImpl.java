package com.tp.user_server2.bank.service.impl;

import com.tp.user_server2.bank.mapper.AccountInfoMapper;
import com.tp.user_server2.bank.model.AccountInfo;
import com.tp.user_server2.bank.service.IAccountInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户表 服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-02-25
 */
@Service
public class AccountInfoServiceImpl extends ServiceImpl<AccountInfoMapper, AccountInfo> implements IAccountInfoService {

}
