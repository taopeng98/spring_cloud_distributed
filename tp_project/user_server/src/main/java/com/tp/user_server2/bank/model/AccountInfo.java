package com.tp.user_server2.bank.model;

import com.baomidou.mybatisplus.extension.activerecord.Model;

/**
 * <p>
 * 账户表
 * </p>
 *
 * @author astupidcoder
 * @since 2021-02-25
 */
public class AccountInfo extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 户主姓名
     */
    private String accountName;

    /**
     * 户主id
     */
    private String accountNo;

    /**
     * 帐户密码
     */
    private String accountPassword;

    /**
     * 帐户余额
     */
    private Double accountBalance;

    public Long getId() {
        return id;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

}
