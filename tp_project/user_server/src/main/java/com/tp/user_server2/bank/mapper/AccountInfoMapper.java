package com.tp.user_server2.bank.mapper;

import com.tp.user_server2.bank.model.AccountInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账户表 Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-02-25
 */
public interface AccountInfoMapper extends BaseMapper<AccountInfo> {

}
