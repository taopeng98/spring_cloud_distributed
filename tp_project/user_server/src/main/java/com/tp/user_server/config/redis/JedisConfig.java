package com.tp.user_server.config.redis;


import com.netflix.servo.util.VisibleForTesting;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "share.redis.jedis.cluster")
public class JedisConfig {

    private String nodes;
    private String password;
    private String maxredirects;
    private String maxactive;
    private String timeout;
    private String maxidle;
    private String minidle;
    private String maxwait;
    private String index;
    private String testborrow;
    private String maxtotal;
    private String testreturn;


    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMaxredirects() {
        return maxredirects;
    }

    public void setMaxredirects(String maxredirects) {
        this.maxredirects = maxredirects;
    }

    public String getMaxactive() {
        return maxactive;
    }

    public void setMaxactive(String maxactive) {
        this.maxactive = maxactive;
    }

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getMaxidle() {
        return maxidle;
    }

    public void setMaxidle(String maxidle) {
        this.maxidle = maxidle;
    }

    public String getMinidle() {
        return minidle;
    }

    public void setMinidle(String minidle) {
        this.minidle = minidle;
    }

    public String getMaxwait() {
        return maxwait;
    }

    public void setMaxwait(String maxwait) {
        this.maxwait = maxwait;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getTestborrow() {
        return testborrow;
    }

    public void setTestborrow(String testborrow) {
        this.testborrow = testborrow;
    }

    public String getMaxtotal() {
        return maxtotal;
    }

    public void setMaxtotal(String maxtotal) {
        this.maxtotal = maxtotal;
    }

    public String getTestreturn() {
        return testreturn;
    }

    public void setTestreturn(String testreturn) {
        this.testreturn = testreturn;
    }
}
