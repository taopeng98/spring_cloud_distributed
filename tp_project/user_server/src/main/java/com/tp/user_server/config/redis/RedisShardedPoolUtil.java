package com.tp.user_server.config.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Component
public class RedisShardedPoolUtil {
    private static Logger log = LoggerFactory.getLogger(RedisShardedPoolUtil.class);
    @Autowired
    private RedisShardedPool redisShardedPool;
    
    public JedisCluster getJedis(){
        return redisShardedPool.initPool();
    }

    /**
     * 设置key的有效期，单位是秒
     * @param key
     * @param exTime
     * @return
     */
    public  Long expire(String key,int exTime){
        JedisCluster jedis = null;
        Long result = null;
        try {
            jedis = getJedis();
            result = jedis.expire(key,exTime);
        } catch (Exception e) {
            log.error("expire key:{} error",key,e);
            return result;
        }
        return result;
    }

    /**
     * 设置key value exTime的单位是秒
     * @param key
     * @param value
     * @param exTime
     * @return
     */
    public  String setEx(String key,String value,int exTime){
        JedisCluster jedis = null;
        String result = null;
        try {
            jedis = getJedis();
            result = jedis.setex(key,exTime,value);
        } catch (Exception e) {
            log.error("setex key:{} value:{} error",key,value,e);
            return result;
        }
        return result;
    }

    /**
     * 获取
     * @param key
     * @return
     */
    public  String get(String key){
        JedisCluster jedis = null;
        String result = null;
        try {
            jedis = getJedis();
            result = jedis.get(key);
        } catch (Exception e) {
            log.error("get key:{} error",key,e);
            return result;
        }

        return result;
    }

    /**
     * 设置值
     * @param key
     * @param value
     * @return
     */
    public  String set(String key,String value){
        JedisCluster jedis = null;
        String result = null;

        try {
            jedis = getJedis();
            result = jedis.set(key,value);
        } catch (Exception e) {
            log.error("set key:{} value:{} error",key,value,e);

            return result;
        }

        return result;
    }

    /**
     * 获取旧的value，设置新的value；可用于锁操作
     * @param key
     * @param value
     * @return
     */
    public  String getSet(String key,String value){
        JedisCluster jedis = null;
        String result = null;

        try {
            jedis = getJedis();
            result = jedis.getSet(key,value);
        } catch (Exception e) {
            log.error("getset key:{} value:{} error",key,value,e);

            return result;
        }

        return result;
    }

    /**
     * 删除key
     * @param key
     * @return
     */
    public  Long del(String key){
        JedisCluster jedis = null;
        Long result = null;
        try {
            jedis = getJedis();
            result = jedis.del(key);
        } catch (Exception e) {
            log.error("del key:{} error",key,e);

            return result;
        }

        return result;
    }

    /**
     * 当且仅当key不存在，将key的值设置为value，并且返回1；
     * 若是给定的key已经存在，则setnx不做任何动作，返回0。
     * 可用于锁操作
     * @param key
     * @param value
     * @return
     */
    public Long setnx(String key,String value){
        JedisCluster jedis = null;
        Long result = null;

        try {
            jedis = getJedis();
            result = jedis.setnx(key,value);
        } catch (Exception e) {
            log.error("setnx key:{} value:{} error",key,value,e);

            return result;
        }

        return result;
    }


    /**
     * 查询有多少KEY
     * @param pattern
     * @return
     */
    public int keys(final String pattern) {
        try {
            final Set<String> keySet = new HashSet();
            final Map<String, JedisPool> nodes = getJedis().getClusterNodes();
            for (String k : nodes.keySet()) {
                JedisPool pool = nodes.get(k);
                //获取Jedis对象，Jedis对象支持keys模糊查询
                Jedis jedis = pool.getResource();
                final Set<String> set = jedis.keys(pattern);
                keySet.addAll(set);
            }

            return keySet.size();
        } catch (Exception e) {
            log.error("异常信息", e);
            throw e;
        }
    }
    /**
     * 模糊查询给定的pattern的所有keys列表
     *
     * @param pattern 模糊查询
     * @return 返回当前pattern可匹配的对象keys列表
     */
    public Set<String> keyList(final String pattern) {

        try {
            final Set<String> keySet = new HashSet<String>();
            final Map<String, JedisPool> nodes = getJedis().getClusterNodes();
            for (String k : nodes.keySet()) {
                JedisPool pool = nodes.get(k);
                //获取Jedis对象，Jedis对象支持keys模糊查询
                Jedis jedis = pool.getResource();
                final Set<String> set = jedis.keys(pattern);
                keySet.addAll(set);
            }
            return keySet;
        } catch (Exception e) {
            log.error("异常信息", e);
            throw e;
        }
    }
}
