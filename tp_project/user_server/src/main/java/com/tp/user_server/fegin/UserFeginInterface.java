package com.tp.user_server.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "USER-SERVER")
public interface UserFeginInterface {

    @GetMapping("//account-info/get")
    public String get();
}
