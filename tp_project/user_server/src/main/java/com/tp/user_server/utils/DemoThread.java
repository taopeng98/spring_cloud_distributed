package com.tp.user_server.utils;

import com.tp.user_server.Test;

public class DemoThread extends Thread {
    @Override
    public void run() {
        sellTicket2();
    }

    /**
     * 模拟售票,多线程下会出现线程安全问题,会发生超卖
     */
    public void sellTicket1(){
        if(Test.num > 0){
            Test.num--;
            System.out.println(Thread.currentThread().getName() + "抢票成功");
        }else{
            System.out.println(Thread.currentThread().getName()+"票已经售罄,不好意思");
        }
    }

    /**
     * 方法上增加java 内部锁(synchronized)
     */
    public synchronized void sellTicket2(){
        if(Test.num > 0){
            Test.num--;
            System.out.println(Thread.currentThread().getName() + "抢票成功");
        }else{
            System.out.println(Thread.currentThread().getName()+"票已经售罄,不好意思");
        }
    }

}
