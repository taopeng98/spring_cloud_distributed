package com.tp.user_server.config.redisson;


import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
@ConfigurationProperties(prefix = "spring.redisson")
//@ConditionalOnProperty("redisson.cluster")
public class RedissonClusterProperties {

    private String password;
    private cluster cluster;

    public static class cluster {
        private List<String> nodes;

        public List<String> getNodes() {
            return nodes;
        }

        public void setNodes(List<String> nodes) {
            this.nodes = nodes;
        }
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RedissonClusterProperties.cluster getCluster() {
        return cluster;
    }

    public void setCluster(RedissonClusterProperties.cluster cluster) {
        this.cluster = cluster;
    }
}
